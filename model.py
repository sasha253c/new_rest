from collections import namedtuple


class User(namedtuple('User', ['email', 'password'])):
    __slots__ = ()

    def __repr__(self):
        return f"Email: {self.email}"


if __name__ == '__main__':
    user = User(email='test_email',
                password='test_password')
    print(user)