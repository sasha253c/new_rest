from flask import Flask

from apis import blueprint

app = Flask(__name__)

app.register_blueprint(blueprint)


@app.route('/')
def hello():
    return "Hello world"


if __name__ == '__main__':
    app.run(debug=True, host='localhost', port=5000)