from flask import Blueprint
from flask_restplus import Api, Resource

from .user import api as user_namespace

blueprint = Blueprint('api', __name__, url_prefix='/api')
api = Api(blueprint, doc='/documentation', version='0.1', title='New API', description='Api desc.')

api.add_namespace(user_namespace)


@api.route('/signup')
class SignUp(Resource):
    """create user (need to include fields "email" and "password" in json dictionary-inside body of POST request)"""
    def post(self):
        print('signup')
        return 'signup'

@api.route('/signin')
class SignIn(Resource):
    """path to authenticate user - fields are "email" and "password" redirect to /profile"""
    def post(self):
        print('signin')
        return 'signin'