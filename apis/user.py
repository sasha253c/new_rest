from flask_restplus import Resource, Namespace, fields, reqparse

api = Namespace('user', path='/user', description='User desc')

email_parser = reqparse.RequestParser(bundle_errors=True)
email_parser.add_argument('email', type=str, required=True, help="email desc")

user_parser = email_parser.copy()
user_parser.add_argument('password', location='headers', type=str, required=True, help="password desc")

user_new_password_parser = email_parser.copy()
user_new_password_parser.add_argument('old_password', location='headers', type=str, required=True, help="old password")
user_new_password_parser.add_argument('new_password', location='headers', type=str, required=True, help="new password")


# user = api.model('User', {
#     'email': fields.String,
#     'password': fields.String,
# })

# USERS = []

@api.route('')
class User(Resource):
    """User"""

    @api.expect(user_new_password_parser, validate=True)
    def put(self):
        args = user_new_password_parser.parse_args()
        email = args['email']
        old_password = args['old_password']
        new_password = args['new_password']

        user = {'email': email, 'password': old_password}
        # check old_password equals in the DB
        print(f'Write to DB: {user}, new password {new_password}')
        return 'OK'

    @api.expect(email_parser, validate=True)
    def get(self):
        args = email_parser.parse_args()
        email = args['email']
        print(f'Read from DB, email {email}')
        return {'email': email}

    @api.expect(user_parser, validate=True)
    def delete(self):
        args = user_parser.parse_args()
        email = args['email']
        password = args['password']
        user = {'email': email, 'password': password}
        print(f'Delete from DB: {user}')
        return 'Delete'


